package pageobject.SavedMailPagetest;

import org.testng.Assert;
import org.testng.annotations.Test;

import pageObject.pages.enterapplication.LoginPage;
import pageObject.pages.enterapplication.PublickMailPage;
import pageObject.pages.enterapplication.SavedMailPage;
import pageobject.testcase.TestBase;

public class SavedMailPageTestSuit extends TestBase {
	@Test
	// TS 1.1
	public void logOutV1() throws InterruptedException {
		LoginPage loginPage = homePage.clickOnSavedEmailsButton();
		PublickMailPage publicMailPage = loginPage.enterLoginDataV1();
		SavedMailPage savedMailPage = publicMailPage.clickOnSavedEmailsButton();
		Assert.assertTrue(savedMailPage.isElementDisplayed(), "Letters is not displayed");
	}
	
	@Test
	// TS 1.2
	public void logOutV2() throws InterruptedException {
		LoginPage loginPage = homePage.clickOnSavedEmailsButton();
		PublickMailPage publicMailPage = loginPage.enterLoginDataV2();
		SavedMailPage savedMailPage = publicMailPage.clickOnSavedEmailsButton();
		Assert.assertTrue(savedMailPage.isElementDisplayed(), "Letters is not displayed");
	}
	
	@Test
	// TS 1.3
	public void logOutV3() throws InterruptedException {
		LoginPage loginPage = homePage.clickOnSavedEmailsButton();
		PublickMailPage publicMailPage = loginPage.enterLoginDataV3();
		SavedMailPage savedMailPage = publicMailPage.clickOnSavedEmailsButton();
		Assert.assertTrue(savedMailPage.isElementDisplayed(), "Letters is not displayed");
	}

}
