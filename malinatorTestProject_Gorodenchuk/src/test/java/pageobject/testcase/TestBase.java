package pageobject.testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import pageObject.pages.enterapplication.HomePage;
import pageObject.pages.webDriver.WebDriverFactory;

//import  pageObject.pages.webDriver.WebDriverFactory;

/*
 * Base class for all the test classes
 */

public class TestBase {

	protected WebDriver webDriver;
	protected pageObject.pages.enterapplication.HomePage homePage;

	@BeforeMethod
	@Parameters({ "browserName" })
	public void setup(String browserName) throws Exception {
		webDriver = WebDriverFactory.getInstance(browserName);
		webDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		// webDriver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		webDriver.manage().window().maximize();
		webDriver.get("https://www.mailinator.com/");
		homePage = PageFactory.initElements(webDriver, HomePage.class);
	}

	@AfterMethod
	public void tearDown() throws Exception {
		if (webDriver != null) {
			WebDriverFactory.killDriverInstance();
		}
	}

}