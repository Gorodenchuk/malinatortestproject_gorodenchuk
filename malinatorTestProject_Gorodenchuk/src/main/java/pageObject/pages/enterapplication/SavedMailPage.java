package pageObject.pages.enterapplication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pageObject.pages.Page;

public class SavedMailPage extends Page {
	
//	@FindBy(how = How.XPATH, using = "//div[@class='someviewport']/div[1]/div[1]/input[@type='checkbox']")
//	private WebElement emailChekBoxElement;   
	
	@FindBy(how = How.XPATH, using = "//div[@class='someviewport']/div/div[@class='col-lg-1 col-md-1 col-sm-1']")
	private WebElement emailChekBoxElement;

	public SavedMailPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isElementDisplayed() {
		try {
			return emailChekBoxElement.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
}
