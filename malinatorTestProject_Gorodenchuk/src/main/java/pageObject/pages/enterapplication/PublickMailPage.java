package pageObject.pages.enterapplication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pageObject.pages.Page;

public class PublickMailPage extends Page{	 
	
	@FindBy(how = How.XPATH, using = "//a[@id='saved_tabarea']")
	private WebElement savedEmailsButton;
	
	public PublickMailPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}
	
	public SavedMailPage clickOnSavedEmailsButton() {
		savedEmailsButton.click();
		return PageFactory.initElements(webDriver, SavedMailPage.class);
	}

}
