package pageObject.pages.enterapplication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pageObject.pages.Page;
import workingWithFiles.PropertyLoader;

public class LoginPage extends Page {
	
	@FindBy(how = How.XPATH, using = "//*[@id='loginEmail']")
	private WebElement emailField;
	
	@FindBy(how = How.XPATH, using = "//*[@id='loginPassword']")
	private WebElement passwordField;
	
	@FindBy(how = How.XPATH, using = "//*[@id='loginpane']//button")
	private WebElement loginButton;

	public LoginPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}
	
	public PublickMailPage enterLoginDataV1() {
		emailField.clear();
		emailField.sendKeys(PropertyLoader.loadProperty("v1_username"));
		passwordField.clear();
		passwordField.sendKeys(PropertyLoader.loadProperty("v1_password"));
		loginButton.click();
		return PageFactory.initElements(webDriver, PublickMailPage.class);
	}
	
	public PublickMailPage enterLoginDataV2() {
		emailField.clear();
		emailField.sendKeys(PropertyLoader.loadProperty("v2_username"));
		passwordField.clear();
		passwordField.sendKeys(PropertyLoader.loadProperty("v2_password"));
		loginButton.click();
		return PageFactory.initElements(webDriver, PublickMailPage.class);
	}
	
	public PublickMailPage enterLoginDataV3() {
		emailField.clear();
		emailField.sendKeys(PropertyLoader.loadProperty("v3_username"));
		passwordField.clear();
		passwordField.sendKeys(PropertyLoader.loadProperty("v3_password"));
		loginButton.click();
		return PageFactory.initElements(webDriver, PublickMailPage.class);
	}
	

}
