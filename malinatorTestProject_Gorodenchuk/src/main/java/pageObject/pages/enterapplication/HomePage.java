package pageObject.pages.enterapplication;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pageObject.pages.Page;

public class HomePage extends Page {
	
	@FindBy(how = How.XPATH, using = "//*[@id='header-1']/nav//ul/li[6]/a")
	private WebElement loginButton;

	public HomePage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}
	
	public LoginPage clickOnSavedEmailsButton() {
		loginButton.click();
		return PageFactory.initElements(webDriver, LoginPage.class);
	}
	
	

}
